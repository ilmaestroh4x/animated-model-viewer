# Animated Model Viewer (Web Test)

![](preview.gif)

## Overview
Animated Model Viewer is a project that looks at implementing a front-end + back-end that displays animated 3d models with very little bandwidth overhead.
The front-end is a C++ project that runs in browser via WASM(Web Assembly) on top of JavaScript bindings to a WebGL canvas renderer.
The back-end consists of a Node.js project that is not only used for the build pipeline, but also server hosting.

The build pipeline is a tool chain that compiles the native C++ source. It then runs the different assets through various methods of compression(minify for css/html/js, gzip/brotli for everything, etc).
Once the build process is over the server can be run. Its setup to scale up a series of hosting clusters based on the CPU cores provided. Whenever a page is requested, it determines which compression formats are accepted by the request header and provides a response using the most compressed (and accepted) version of the file.

## Getting Started

The following instructions will get you up and running to try this project out on your own system.

### Prerequisites

To host (NOT build) you'll need:

* [Nodejs](https://nodejs.org/en/) - (required) The environment with which the project is built 
* [git](https://git-scm.com/) - (optional) If you wish to clone this project, you can also just download the repo instead
* [VSCode](https://code.visualstudio.com/) - (optional) Useful text editor if you wish to delve into the code and don,t have an editor of choice already.

If using git, you can use the command:
```
git clone https://gitlab.com/ilmaestroh4x/animated-model-viewer.git
```
The project comes with a prebuilt project that is ready to be hosted, however, if you also wish to run the build pipeline, you'll need:
* [Emscripten](https://emscripten.org/docs/getting_started/downloads.html) - (required) Used for the C++ compilation. (Setup is outside the scope of this project.)
* [VSCode](https://code.visualstudio.com/) - (required) For easily running the `Build` task. (If you're an advanced user, you can probably do without this, however, any additional setup is outside the scope of this project.)

The additional dependancies for Emscripten are not listed here. It's assumed you already have emscripten fully setup and working with everything it needs.

### Installing NPM dependencies

Any required node modules can be installed using the `npm i` command. This will grab all required packages.

### Running project

If you're just running the prebuild project all you need to do is `cd` into the root project folder and run `npm start` if you're not using VSCode. If you are, however, just open the project and hit run in VSCode.
Once the project has been started, open a browser (that supports WebGL 2.0 or higher, almost anything other then MS Edge & IE) and go to `http://localhost:8080/` to view the animated 3d model.

You can 'fly around' by holding down right click and using W,A,S,D to move (typical PC FPS controls). Hold down shift to 'sprint'. Input will lock whenever right click is released.

If you also wish to build the project (run the build toolchain, compilation > minification > compression) open VSCode, click `Terminal` at the top, and select `Run Task...` from the dropdown and click `build` from the popup.

## Additional Notes

This project uses custom binary model file format(.wmdl 'Web Model') that was generated from a separate project, adding your own models is beyond the scope of this project.
The reason for this is to provide a single file format that is easily parsable without requiring additional libraries such as [Assimp](https://www.assimp.org/) which have a lot of unwanted overhead to support many file types.

## License

See 'LICENSE' in project root.