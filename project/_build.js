;(async () => {
    'use strict'
    
    const is_debug = false

    const res_dir = './resources/'
    const src_dir = './src/'
    const bld_dir = './build/'
    const rls_dir = './release/'

    const NO_ENCODE_TYPES = [
        '.woff',
        '.woff2'
    ]

    const zlib  = require('zlib')
    const path  = require('path')
    const util  = require('util')
    const fs    = require('fs')
    const exec  = util.promisify(require('child_process').exec)

    const unlink = util.promisify(fs.unlink)

    //const iltorb = require('iltorb') replaced w/ zlibs brotli compress
    const babel = require('babel-core')
    const minify = require('html-minifier').minify
    const str = require('string-to-stream')
    //const xml2js = require('xml2js')

    function find_all_files(initial_path, ext_types, on_file_found) {
        if (!(ext_types instanceof Array)) return;
        ;(function _fn(p) {
            fs.readdirSync(p).forEach(f => {
                const newp = path.join(p, f)
                const stats = fs.statSync(newp)
                if (stats.isFile()) {
                    const ext = path.extname(newp).slice(1)
                    if (ext_types.length === 0 || ext_types.indexOf(ext) >= 0) {
                        on_file_found(newp)
                    }
                } else if (stats.isDirectory()) {
                    _fn(newp)
                }
            })
        })(initial_path)
    }

    function copy_file(cur, out) {
        return new Promise((resolve, reject) => {
            fs.createReadStream(cur)
            .pipe(fs.createWriteStream(out))
            .on('finish', () => resolve())
            .on('error', () => reject())
        })
    }

    try {
        console.log(`Build Started - ${new Date().toString()}`)

        console.log('Preparing Directorties...')
        if (!fs.existsSync(bld_dir)) fs.mkdirSync(bld_dir)
        if (!fs.existsSync(rls_dir)) fs.mkdirSync(rls_dir)
        console.log('finished')

//--------------------------------------------------------------------------------------------------------------------------------

        console.log('Deleting old files...')
        const files_to_delete = []
        find_all_files(bld_dir, [/*ALL*/], p => {
            files_to_delete.push(p)
        })
        find_all_files(rls_dir, [/*ALL*/], p => {
            files_to_delete.push(p)
        })
        console.log(files_to_delete)
        for (let i = 0; i < files_to_delete.length; i++) {
            try {
                await unlink(files_to_delete[i])
            } catch (err) {
                console.log(err)
            }
        }
        console.log('finished')

//--------------------------------------------------------------------------------------------------------------------------------

        console.log('Moving resources...')
        const files_to_copy = []
        find_all_files(res_dir, [/*ALL*/], p => {
            files_to_copy.push([p, path.join(bld_dir, path.basename(p))])
        })
        for (let i = 0; i < files_to_copy.length; i++) {
            await copy_file(...files_to_copy[i])
        }
        console.log('finished')

//--------------------------------------------------------------------------------------------------------------------------------

        console.log('Building sources...')
        const sources = []
        find_all_files(src_dir, ['cpp', 'CPP'], p => {
            sources[p.toLowerCase().indexOf('main.') >= 0 ? 'unshift' : 'push'](p)
        })
        console.log(sources)

        const { stdout, stderr } = await exec([
            `em++`,
            `-std=c++1z`,
            //`-Werror`,
            `-I${src_dir}native/libs/`,
            `${sources.join(' ')}`,
            `${is_debug ? '' : '-Oz'}`,
            //`${is_debug ? '' : '--js-opts 1'}`,
            //`${is_debug ? '' : '-s MODULARIZE=1'}`,
            `${is_debug ? '' : '--closure 1'}`, // currently broken, using a patch in minify stage to fix.
            //`${is_debug ? '' : '--llvm-opts 3'}`,
            `${is_debug ? '' : '--llvm-lto 1'}`,
            `${is_debug ? '-D _DEBUG' : ''}`,
            `${is_debug ? '-g3' : ''}`,
            `${is_debug ? '-s DEMANGLE_SUPPORT=1' : ''}`,
            `${is_debug ? '-s ASSERTIONS=2' : ''}`,
            `${is_debug ? '-s SAFE_HEAP=1' : ''}`,
            `${is_debug ? '-s ALIASING_FUNCTION_POINTERS=0' : ''}`,
            `${is_debug ? '-s STACK_OVERFLOW_CHECK=2' : ''}`,
            //`${is_debug ? '--memoryprofiler' : ''}`,
            //`${is_debug ? '--cpuprofiler' : ''}`,
            `${is_debug ? '-s DISABLE_EXCEPTION_CATCHING=2' : ''}`,
            //`${is_debug ? '-s LEGACY_GL_EMULATION=1' : ''}`,
            //`${is_debug ? '-s GL_UNSAFE_OPTS=0' : ''}`,
            //`-D _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS`,
            `-s ALLOW_MEMORY_GROWTH=1 --no-heap-copy`, //for larger models
            `-s USE_WEBGL2=1`,
            //`-s FULL_ES3=1`,
            //`-s USE_GLFW=3`,
            `-s WASM=1`,
            `-s ENVIRONMENT=web`,
            `-o ${bld_dir}index.html`,
            `--preload-file ${src_dir}assets`,
            //`--pre-js ${res_dir}pre.js`, // would be nice to use, but doesn't seem to play nice with closure compiler.
            `--shell-file ${res_dir}index.html`
        ].join(' '), {
            maxBuffer: 1000 * 1024
        })

        if (stdout) console.log('stdout:\n', stdout)
        if (stderr) console.log('stderr:\n', stderr)
        console.log('finished')

//--------------------------------------------------------------------------------------------------------------------------------

        console.log('Compressing files...')
        const files_to_compress = []
        find_all_files(bld_dir, [/*ALL*/], p => {
            files_to_compress.push(p)
        })
        for (let i = 0; i < files_to_compress.length; i++) {
            const p = files_to_compress[i]

            let source
            switch (path.extname(p) + (is_debug ? '-debug' : '')) {
                case '.html': {
                    source = minify(fs.readFileSync(p).toString(), {
                        html5: true,
                        minifyCSS: true,
                        minifyURLs: true,
                        removeAttributeQuotes: true,
                        removeComments: true,
                        removeOptionalTags: true,
                        removeRedundantAttributes: true,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true,
                        collapseBooleanAttributes: true,
                        collapseWhitespace: true,
                        collapseInlineTagWhitespace: true,
                        removeAttributeQuotes: true,
                        useShortDoctype: true,
                        minifyJS: function(text, inline) {
                            return babel.transform(text, {
                                presets: [ "es2016", "minify" ]
                            }).code
                        }
                    })
                    source = str(source)
                } break
                case '.js': {
                    source = babel.transformFileSync(p, {
                        presets: [ "es2016", "minify" ]
                    }).code

                    // fix closure compiler error
                    if (p.indexOf('index.js') >= 0) {
                        const regex = /_glVertexAttribIPointer:function\([a-z,]+\){GLctx\.([a-z,]+)/g
                        const subst = `vertexAttribIPointer`
                        const result = regex.exec(source)
                        const patch = result[0].replace(result[1], subst)
                        console.log(`PATCHING FILE >> ${p} << : >> ${result[0]}  << PATCHED TO >>  ${patch}`)
                        source = source.replace(result[0], patch)
                    }

                    source = str(source)
                } break
                //case '.xml': {
                //    source = fs.readFileSync(p).toString()
                //    const parseXMLString = async (source) => {
                //        return new Promise((resolve, reject) => {
                //            xml2js.parseString(source, (err, response) => {
                //                if (err) reject(err)
                //                else resolve(response)
                //            })
                //        })
                //    }
                //    let json_from_xml = await parseXMLString(source)
                //    const precision = 4
                //    json_from_xml = JSON.parse(JSON.stringify(json_from_xml, (key, value) => {
                //        if (!isNaN(value) && value.toString().indexOf('.') != -1) {
                //            return parseFloat(parseFloat(value).toFixed(precision))
                //        }
                //        return value;
                //    }))
                //    source = new xml2js.Builder().buildObject(json_from_xml)
                //    source = str(source)
                //} break
                default: {
                    source = fs.createReadStream(p)
                } break
            }

            const file_path = path.join(rls_dir, path.basename(p))
            
            if (NO_ENCODE_TYPES.indexOf(path.extname(p).toLowerCase()) === -1) {

                source
                .pipe(zlib.createBrotliCompress())
                .pipe(fs.createWriteStream(`${file_path}.br`))

                source
                .pipe(zlib.createGzip())
                .pipe(fs.createWriteStream(`${file_path}.gzip`))

            } else {

                source
                .pipe(fs.createWriteStream(file_path))

            }
        }
        console.log('finished')

//--------------------------------------------------------------------------------------------------------------------------------

    console.log('TODO: use Uniform Buffer Objects in WebGL 2.')

        console.log('Starting server...')
        await exec(`node ./_cluster.js`)

    } catch (err) {
        console.log(err)
    }
})()