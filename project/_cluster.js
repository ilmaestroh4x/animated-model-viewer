'use strict'

const cluster = require('cluster')
const os      = require('os')

if (cluster.isMaster) {
    os.cpus().forEach(cpu => cluster.fork())

    cluster.on('exit', (worker, code, signal) => {
        if (code !== 0 && !worker.exitedAfterDisconnect) {
            cluster.fork()
        }
    })
} else {
    require('./_server')
}