'use strict'

const http = require('http')
const path = require('path')
const fs   = require('fs')

// Mime-Type mappings
const MIME_TYPE = {
    '.html' : 'text/html',
    '.bin'  : 'text/plain',
    '.txt'  : 'text/plain',
    '.css'  : 'text/css',
    '.html' : 'text/html',
    '.png'  : 'image/png',
    '.gif'  : 'image/gif',
    '.jpg'  : 'image/jpeg',
    '.jpeg' : 'image/jpeg',
    '.ico'  : 'image/x-icon',
    '.js'   : 'text/javascript',
    '.mp4'  : 'video/mp4',
    '.ttf'  : 'font/ttf',
    '.woff' : 'font/woff',
    '.woff2': 'font/woff2',
    '.json' : 'application/json',
    '.wasm' : 'application/wasm',
    '.data' : 'application/octet-stream',
    '.mem'  : 'application/octet-stream',
}

const NO_ENCODE_TYPES = [
    '.woff',
    '.woff2'
]

// Response encoding order, if accept type is not on list, use no encoding.
const ENCODING_PRIORITY = [
    'br',
    'gzip',
    ''//none
]
//https://kripken.github.io/emscripten-site/docs/porting/files/Synchronous-Virtual-XHR-Backed-File-System-Usage.html#synchronous-virtual-xhr-backed-file-system-usage
const cwd = path.join(process.cwd(), 'release')

const port = 8080
http.createServer((req, res) => {
    switch (req.method) {
        case 'GET': {

            let url = req.url
            if (url === '/')
                url = '/index.html'
            
            let ext_type = path.extname(url).toLowerCase()

            let use_encoding = NO_ENCODE_TYPES.indexOf(ext_type) === -1

            let encoding = ''
            let file_name = url

            if (use_encoding) {
                const accept_encodings = req.headers['accept-encoding'].split(/\s*,\s*/g)
                for (let i = 0; i < ENCODING_PRIORITY.length; i++) {
                    encoding = ENCODING_PRIORITY[i]
                    if (accept_encodings.indexOf(encoding) >= 0) break
                }

                if (encoding === '') {
                    use_encoding = false
                } else {
                    file_name += `.${encoding}`
                }
            }

            let file = path.join(cwd, file_name)

            fs.stat(file, (err, stats) => {
                let status_code = 200

                if (err) {
                    file_name = '404.html'
                    ext_type = '.html'

                    if (use_encoding) {
                        file_name += `.${encoding}`
                    }

                    status_code = 404
                    file = path.join(cwd, file_name)
                    stats = fs.statSync(file)
                }

                let headers = {
                    'Content-Type': MIME_TYPE[ext_type],
                    'Content-Length': stats.size
                }

                if (use_encoding) {
                    headers['Content-Encoding'] = encoding
                }

                if (ext_type !== '.html' || err) {
                    const day_in_seconds = 60 * 60 * 24
                    headers['Cache-Control'] = `public, max-age=${day_in_seconds}`
                }

                res.writeHead(status_code, headers)
                fs.createReadStream(file).pipe(res)
            })

        } break
    }
}).listen(port, err => {
    console.log(err ? `Error: ${err}` : `Server shard started.`)
})