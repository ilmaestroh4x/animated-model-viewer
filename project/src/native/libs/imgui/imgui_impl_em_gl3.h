#ifdef __EMSCRIPTEN__
#pragma once

#include <emscripten/emscripten.h>
#include <emscripten/html5.h>

IMGUI_API bool  ImGui_ImplEMGL3_Init(bool install_callbacks);
IMGUI_API void  ImGui_ImplEMGL3_Shutdown();
IMGUI_API void  ImGui_ImplEMGL3_NewFrame();
IMGUI_API void  ImGui_ImplEMGL3_RenderDrawData(ImDrawData* draw_data);

// Use if you want to reset your rendering device without losing ImGui state.
IMGUI_API void  ImGui_ImplEMGL3_InvalidateDeviceObjects();
IMGUI_API bool  ImGui_ImplEMGL3_CreateDeviceObjects();

// Emscripten callbacks (installed by default if you enable 'install_callbacks' during initialization)
// Provided here if you want to chain callbacks.
// You can also handle inputs yourself and use those as a reference.
IMGUI_API EM_BOOL   ImGui_ImplEM_MouseCallback(int eventType, const EmscriptenMouseEvent* mouseEvent, void* userData);
IMGUI_API EM_BOOL   ImGui_ImplEM_ScrollCallback(int eventType, const EmscriptenWheelEvent* wheelEvent, void* userData);
IMGUI_API EM_BOOL   ImGui_ImplEM_KeyCallback(int eventType, const EmscriptenKeyboardEvent* keyEvent, void* userData);

#endif