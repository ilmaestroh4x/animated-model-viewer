#include "camera.h"

#include <glm/gtc/matrix_transform.hpp>

using namespace core;

camera::camera() :
	m_viewChanged{false},
	m_eye{0.0f, 0.0f, 1.0f},
	m_target{0.0f, 0.0f, 0.0f},
	m_up{0.0f, 1.0f, 0.0f},
	m_view{glm::lookAt(m_eye, m_target, m_up)},

	m_invViewChanged{false},
	m_invView{glm::inverse(m_view)},

	m_projChanged{false},
	m_fov{glm::radians(65.0f)},
	m_aspect{16.0f / 9.0f},
	m_near{0.5f},
	m_far{500.0f},
	m_proj{glm::perspective(m_fov, m_aspect, m_near, m_far)},

	m_invProjChanged{false},
	m_invProj{glm::inverse(m_proj)},

	m_viewProjChanged{false},
	m_viewProj{m_proj * m_view},

	m_invViewProjChanged{false},
	m_invViewProj{glm::inverse(m_viewProj)} {
}

const glm::mat4& camera::get_view_matrix() {
	if (m_viewChanged) {
		m_viewChanged = false;
		m_invViewChanged = true;
		m_viewProjChanged = true;

		m_view = glm::lookAt(m_eye, m_target, m_up);
	}
	return m_view;
}

const glm::mat4& camera::get_inv_view_matrix() {
	// Validate "m_view".
	get_view_matrix();

	if (m_invViewChanged) {
		m_invViewChanged = false;

		m_invView = glm::inverse(m_view);
	}
	return m_invView;
}

const glm::mat4& camera::get_proj_matrix() {
	if (m_projChanged) {
		m_projChanged = false;
		m_invProjChanged = true;
		m_viewProjChanged = true;

		m_proj = glm::perspective(m_fov, m_aspect, m_near, m_far);
	}
	return m_proj;
}

const glm::mat4& camera::get_inv_proj_matrix() {
	// Validate "m_proj".
	get_proj_matrix();

	if (m_invProjChanged) {
		m_invProjChanged = false;

		m_invProj = glm::inverse(m_proj);
	}
	return m_invProj;
}

const glm::mat4& camera::get_view_proj_matrix() {
	// Validate "m_proj".
	get_proj_matrix();

	// Validate "m_view".
	get_view_matrix();

	// If ether "m_proj" or "m_view" changed during validation, "m_viewProjChanged" will be true.
	if (m_viewProjChanged) {
		m_viewProjChanged = false;
		m_invViewProjChanged = true;

		m_viewProj = m_proj * m_view;
	}
	return m_viewProj;
}

const glm::mat4& camera::get_inv_view_proj_matrix() {
	// Validate "m_viewProj".
	get_view_proj_matrix();

	if (m_invViewProjChanged) {
		m_invViewProjChanged = false;

		m_invViewProj = glm::inverse(m_viewProj);
	}
	return m_invViewProj;
}
