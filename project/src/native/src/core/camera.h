#pragma once

#include <glm/glm.hpp>

namespace core {

	class camera {

		/*View*/
		bool m_viewChanged;
		glm::vec3 m_eye;
		glm::vec3 m_target;
		glm::vec3 m_up;
		glm::mat4 m_view;

		/*Inverse View*/
		bool m_invViewChanged;
		glm::mat4 m_invView;

		/*Projection*/
		bool m_projChanged;
		float m_fov;
		float m_aspect;
		float m_near;
		float m_far;
		glm::mat4 m_proj;

		/*Inverse Projection*/
		bool m_invProjChanged;
		glm::mat4 m_invProj;

		/*View Projection*/
		bool m_viewProjChanged;
		glm::mat4 m_viewProj;

		/*Inverse View Projection*/
		bool m_invViewProjChanged;
		glm::mat4 m_invViewProj;

	public:

		camera();

		inline void set_eye(const glm::vec3& _eye) {
			m_viewChanged = true;
			m_eye = _eye;
		}

		inline void set_target(const glm::vec3& _target) {
			m_viewChanged = true;
			m_target = _target;
		}

		inline void set_up(const glm::vec3& _up) {
			m_viewChanged = true;
			m_up = _up;
		}

		inline void set_fov(const float _degrees) {
			m_projChanged = true;
			m_fov = glm::radians(_degrees);
		}

		inline void set_aspect(const float _aspect) {
			m_projChanged = true;
			m_aspect = _aspect;
		}

		inline void set_znear(const float _near) {
			m_projChanged = true;
			m_near = _near;
		}

		inline void set_zfar(const float _far) {
			m_projChanged = true;
			m_far = _far;
		}

		const glm::mat4& get_view_matrix();

		const glm::mat4& get_inv_view_matrix();

		const glm::mat4& get_proj_matrix();

		const glm::mat4& get_inv_proj_matrix();

		const glm::mat4& get_view_proj_matrix();

		const glm::mat4& get_inv_view_proj_matrix();

		inline void on_window_resized(const int _width, const int _height) {
			set_aspect(static_cast<float>(_width) / static_cast<float>(_height));
		}

	};

};