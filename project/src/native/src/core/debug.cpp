#ifndef __EMSCRIPTEN__
#include "debug.h"

#include <GL/glew.h>

#include <glm/gtc/type_ptr.hpp>

#include "../gl.h"

using namespace core;

void debug::draw_mat4(const glm::mat4& _matrix) {
	glm::vec4 origin = _matrix * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	glm::vec4 direction;

	draw_point(origin);

	direction = _matrix * glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	draw_line(origin, direction, color_red);

	direction = _matrix * glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
	draw_line(origin, direction, color_green);

	direction = _matrix * glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	draw_line(origin, direction, color_blue);
}

void debug::draw_frustum(const glm::mat4& _projectionMatrix, const glm::mat4& _viewMatrix) {
	const glm::mat4& invViewProj = glm::inverse(_projectionMatrix * _viewMatrix);

	cube_vertices frustumVertices;
	for (size_t i = 0; i < cube_vertex_count; i++) {
		frustumVertices[i] = invViewProj * clip_cube_vertices[i];
		frustumVertices[i] /= frustumVertices[i].w;
	}

	draw_mat4(glm::inverse(_viewMatrix));

	draw_line(frustumVertices[0], frustumVertices[1]);
	draw_line(frustumVertices[1], frustumVertices[2]);
	draw_line(frustumVertices[2], frustumVertices[3]);
	draw_line(frustumVertices[3], frustumVertices[0]);

	draw_line(frustumVertices[4], frustumVertices[5]);
	draw_line(frustumVertices[5], frustumVertices[6]);
	draw_line(frustumVertices[6], frustumVertices[7]);
	draw_line(frustumVertices[7], frustumVertices[4]);

	draw_line(frustumVertices[0], frustumVertices[4]);
	draw_line(frustumVertices[1], frustumVertices[5]);
	draw_line(frustumVertices[2], frustumVertices[6]);
	draw_line(frustumVertices[3], frustumVertices[7]);
}

void debug::on_render(const glm::mat4& _projectionMatrix, const glm::mat4& _viewMatrix) {
	if (m_points.empty() && m_lines.empty()) return;

	gl(Disable(GL_DEPTH_TEST));

	gl(MatrixMode(GL_PROJECTION));
	gl(LoadMatrixf(glm::value_ptr(_projectionMatrix)));

	gl(MatrixMode(GL_MODELVIEW));
	gl(LoadMatrixf(glm::value_ptr(_viewMatrix)));

	gl(PointSize(5.0f));

	gl(Begin(GL_POINTS));
	for (const point& point : m_points) {
		gl(Color3fv(glm::value_ptr(point.color)));
		gl(Vertex3fv(glm::value_ptr(point.position)));
	}
	gl(End());

	gl(Begin(GL_LINES));
	for (const line& line : m_lines) {
		gl(Color3fv(glm::value_ptr(line.color)));
		gl(Vertex3fv(glm::value_ptr(line.beginPosition)));

		gl(Color3fv(glm::value_ptr(line.color)));
		gl(Vertex3fv(glm::value_ptr(line.endPosition)));
	}
	gl(End());

	m_points.clear();
	m_lines.clear();

	gl(Enable(GL_DEPTH_TEST));
}
#endif