#pragma once

#include <array>
#include <vector>

#include <glm/glm.hpp>

namespace core {

	class debug {

		static constexpr unsigned int cube_vertex_count = 8;

		using cube_vertices = std::array<glm::vec4, cube_vertex_count>;

		inline static const cube_vertices clip_cube_vertices = {
			glm::vec4(1.0f, 1.0f, 0.0f, 1.0f),
			glm::vec4(-1.0f, 1.0f, 0.0f, 1.0f),
			glm::vec4(-1.0f,-1.0f, 0.0f, 1.0f),
			glm::vec4(1.0f,-1.0f, 0.0f, 1.0f),
			glm::vec4(1.0f, 1.0f, 1.0f, 1.0f),
			glm::vec4(-1.0f, 1.0f, 1.0f, 1.0f),
			glm::vec4(-1.0f,-1.0f, 1.0f, 1.0f),
			glm::vec4(1.0f,-1.0f, 1.0f, 1.0f)
		};

		struct point {
			glm::vec3 position;
			glm::vec3 color;
		};

		struct line {
			glm::vec3 beginPosition;
			glm::vec3 endPosition;
			glm::vec3 color;
		};

		std::vector<point> m_points;
		std::vector<line> m_lines;

	public:

		inline static const glm::vec3 color_white = glm::vec3(1.0f);
		inline static const glm::vec3 color_red = glm::vec3(1.0f, 0.0f, 0.0f);
		inline static const glm::vec3 color_green = glm::vec3(0.0f, 1.0f, 0.0f);
		inline static const glm::vec3 color_blue = glm::vec3(0.0f, 0.0f, 1.0f);

		inline void draw_point(const glm::vec3& _point, const glm::vec3& _color = color_white) {
			m_points.push_back({_point, _color});
		}

		inline void draw_line(const glm::vec3& _point_0, const glm::vec3& _point_1, const glm::vec3& _color = color_white) {
			m_lines.push_back({_point_0, _point_1, _color});
		}

		void draw_mat4(const glm::mat4& _matrix);

		void draw_frustum(const glm::mat4& _projectionMatrix, const glm::mat4& _viewMatrix);

		void on_render(const glm::mat4& _projectionMatrix, const glm::mat4& _viewMatrix);

	};

};