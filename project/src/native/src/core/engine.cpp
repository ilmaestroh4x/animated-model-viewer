#include "engine.h"

#include <string>
#include <memory>
#include <fstream>
//#include <utility>

#define CEREAL_FUTURE_EXPERIMENTAL

#include <cereal/types/memory.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/adapters.hpp>
#include <cereal/archives/portable_binary.hpp>

//#include "../assimp/loader.h"

#include "../model/user_data.h"

#include "../model/scene.h"

#include "../gl.h"
#include "../logger.h"

//#include "debug.h"

namespace glm {

	template<typename Archive>
	void serialize(Archive& _archive, glm::vec2& _vec2) {
		_archive(_vec2[0], _vec2[1]);
	}

	template<typename Archive>
	void serialize(Archive& _archive, glm::vec3& _vec3) {
		_archive(_vec3[0], _vec3[1], _vec3[2]);
	}

	template<typename Archive>
	void serialize(Archive& _archive, glm::vec4& _vec4) {
		_archive(_vec4[0], _vec4[1], _vec4[2], _vec4[3]);
	}

	template<typename Archive>
	void serialize(Archive& _archive, glm::ivec4& _ivec4) {
		_archive(_ivec4[0], _ivec4[1], _ivec4[2], _ivec4[3]);
	}

	template<typename Archive>
	void serialize(Archive& _archive, glm::quat& _quat) {
		_archive(_quat[0], _quat[1], _quat[2], _quat[3]);
	}

	template<typename Archive>
	void serialize(Archive& _archive, glm::mat4& _mat4) {
		_archive(
			_mat4[0][0], _mat4[0][1], _mat4[0][2], _mat4[0][3],
			_mat4[1][0], _mat4[1][1], _mat4[1][2], _mat4[1][3],
			_mat4[2][0], _mat4[2][1], _mat4[2][2], _mat4[2][3],
			_mat4[3][0], _mat4[3][1], _mat4[3][2], _mat4[3][3]
		);
	}

}

using namespace core;

engine::engine() {
	
	auto file = "src/assets/deathstroke.wmdl";
	/*
	{	
		// save
		std::ofstream os(file, std::ios::binary);
		cereal::PortableBinaryOutputArchive archiveSave(os);

		//model m = assimp::loader::load_model("models/vanille/happy.fbx");
		//model m = assimp::loader::load_model("models/drone/BusterDrone.fbx");
		model::scene scene = assimp::loader::load_model("models/stormtrooper/stormtrooper.fbx");
		std::unique_ptr<model::scene> scene_ptr = std::make_unique<model::scene>(scene);
		archiveSave(scene_ptr);
	}
	//*/

	//*
	{
		//https://github.com/USCiLab/cereal/issues/46
		//https://github.com/USCiLab/cereal/blob/5715d7ad01429eb9991ff6cd5369ec2054bfbd0c/include/cereal/archives/adapters.hpp

		model::user_data userData(file);

		std::ifstream is(file, std::ios::binary);
		cereal::UserDataAdapter<model::user_data, cereal::PortableBinaryInputArchive> archiveLoad(userData, is);

		std::unique_ptr<model::scene> scene = nullptr;
		archiveLoad(scene);
		m_modelRenderers.push_back(*scene);

	}
	//*/
	/*
	{	
		// load
		std::ifstream is(file, std::ios::binary);
		cereal::PortableBinaryInputArchive archiveLoad(is);

		std::unique_ptr<model::scene> scene = nullptr;
		archiveLoad(scene);
		m_modelRenderers.push_back(*scene);
	}
	//*/

	//m_modelRenderers.push_back(assimp::loader::load_model("models/stormtrooper/stormtrooper.fbx"));

	gl(Enable(GL_DEPTH_TEST));
	gl(DepthFunc(GL_LESS));

	gl(Enable(GL_CULL_FACE));
	gl(CullFace(GL_BACK));

	gl(ClearColor(0.1f, 0.1f, 0.1f, 0.0f));
}

void engine::on_window_resized(int _width, int _height) {
	m_fps_camera.on_window_resized(_width, _height);
	gl(Viewport(0, 0, _width, _height));
}

void engine::on_mouse_button(int _button, int _action, int _mods) {
	m_fps_camera.on_mouse_button(_button, _action, _mods);
}

void engine::on_cursor_moved(float _x, float _y) {
	m_fps_camera.on_cursor_moved(_x, _y);
}

void engine::on_key_input(int _key, int _action) {
	m_fps_camera.on_key_input(_key, _action);
}

void engine::on_update(float _globalTime, float _timeStep) {
	m_fps_camera.on_update(_timeStep);
}

void engine::on_render(float _globalTime) {
	gl(Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

	m_fps_camera.on_render();

	auto& viewProj = m_fps_camera.get_camera().get_view_proj_matrix();

	for (auto& modelRenderer_ : m_modelRenderers) {
		modelRenderer_.animate(0, _globalTime);
		modelRenderer_.render(viewProj, m_shader);
	}
}

void engine::on_gui() {

}
