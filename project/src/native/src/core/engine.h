#pragma once

#include <vector>

#include "shader.h"
#include "fps_camera_controller.h"

#include "../model/scene_renderer.h"

namespace core {

	class engine {

		fps_camera_controller m_fps_camera;

		shader m_shader;

		std::vector<model::scene_renderer> m_modelRenderers;

	public:

		engine();

		void on_window_resized(int _width, int _height);

		void on_mouse_button(int _button, int _action, int _mods);

		void on_cursor_moved(float _x, float _y);

		void on_key_input(int _key, int _action);

		void on_update(float _globalTime, float _timeStep);

		void on_render(float _globalTime);

		void on_gui();

	};

};