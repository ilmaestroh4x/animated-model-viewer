#include "fps_camera_controller.h"

#include <cmath>
#include <algorithm>

#ifdef __EMSCRIPTEN__
#define GLFW_PRESS          2
#define GLFW_RELEASE        3

#define GLFW_KEY_LEFT_SHIFT -1397911671
#define GLFW_KEY_W          2335224
#define GLFW_KEY_A          2335202
#define GLFW_KEY_S          2335220
#define GLFW_KEY_D          2335205
#else
#include <GLFW/glfw3.h>
#endif

using namespace core;

const glm::vec3 fps_camera_controller::vec3_rt = glm::vec3(1, 0, 0);
const glm::vec3 fps_camera_controller::vec3_up = glm::vec3(0, 1, 0);
const glm::vec3 fps_camera_controller::vec3_fw = glm::vec3(0, 0, 1);

fps_camera_controller::fps_camera_controller() {
	m_locRotUp = vec3_up;
	m_locRotFw = vec3_fw;
	m_position = {0, 2, 5};
	m_moveSpeed = walk_speed;
}

void fps_camera_controller::on_window_resized(int _width, int _height) {
	m_camera.on_window_resized(_width, _height);
}

void fps_camera_controller::on_mouse_button(int _button, int _action, int _mods) {
}

void fps_camera_controller::on_cursor_moved(float _x, float _y) {
	//glm::vec2 newCoords = glm::vec2(_x, _y);
	//m_rotDelta = m_oldCoords - newCoords;
	//m_oldCoords = newCoords;

	//float length = glm::length(m_rotDelta);
	//if (length > 150.0f) {
	//	// Prevent jump when mouse locks and unlocks quickly while being dragged.
	//	length = 0;
	//}

	m_rotDelta = glm::vec2(_x, _y);
	float length = glm::length(m_rotDelta);
	if (length > 0) {
		m_rotDelta /= length;
		m_rotDelta *= length * look_speed;

		m_rotAccumulator += m_rotDelta;

		m_rotAccumulator.x = fmod(m_rotAccumulator.x, 360.0f);
		m_rotAccumulator.y = std::clamp(m_rotAccumulator.y, -90.0f, 90.0f);

		m_rotCurrent =
			glm::angleAxis(glm::radians(m_rotAccumulator.x), vec3_up) *
			glm::angleAxis(glm::radians(m_rotAccumulator.y), vec3_rt);

		m_rotCurrent *= m_rotInitial;

		m_locRotRt = m_rotCurrent * vec3_rt;
		m_locRotUp = m_rotCurrent * vec3_up;
		m_locRotFw = m_rotCurrent * vec3_fw;
	}
}

void fps_camera_controller::on_key_input(int _key, int _action) {
	if (_action != GLFW_PRESS && _action != GLFW_RELEASE) return;

	float value = _action == GLFW_RELEASE ? -1.0f : 1.0f;
	bool inputChanged = true;

	switch (_key) {
	default: inputChanged = false;
	case GLFW_KEY_LEFT_SHIFT:
		m_moveSpeed = _action == GLFW_RELEASE ? walk_speed : run_speed;
		break;
	case GLFW_KEY_W:
		m_inputVec.z -= value;
		break;
	case GLFW_KEY_A:
		m_inputVec.x -= value;
		break;
	case GLFW_KEY_S:
		m_inputVec.z += value;
		break;
	case GLFW_KEY_D:
		m_inputVec.x += value;
		break;
	}

	if (inputChanged) {
		m_inputVec.x = std::clamp(m_inputVec.x, -1.0f, 1.0f);
		m_inputVec.z = std::clamp(m_inputVec.z, -1.0f, 1.0f);

		m_inputMagnitude = glm::length(m_inputVec);
	}
}

void fps_camera_controller::on_update(float _timeStep) {
	if (m_inputMagnitude > 0) {
		m_moveVec = m_inputVec / m_inputMagnitude;
		m_moveVec *= m_moveSpeed * _timeStep;

		m_moveStrafe = m_locRotRt * m_moveVec.x;
		m_moveForward = m_locRotFw * m_moveVec.z;

		m_position += m_moveStrafe;
		m_position += m_moveForward;
	}
}

void fps_camera_controller::on_render() {
	m_camera.set_eye(m_position);
	m_camera.set_target(m_position - m_locRotFw);
	m_camera.set_up(m_locRotUp);
}
