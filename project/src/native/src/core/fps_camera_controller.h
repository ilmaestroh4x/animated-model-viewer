#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "camera.h"

namespace core {

	class fps_camera_controller {

		static const glm::vec3 vec3_rt;
		static const glm::vec3 vec3_up;
		static const glm::vec3 vec3_fw;

		static constexpr float look_speed = 0.15f;

		static constexpr float walk_speed = 5.0f;
		static constexpr float run_speed = 15.0f;

		//glm::vec2 m_oldCoords;

		glm::vec2 m_rotDelta;
		glm::vec2 m_rotAccumulator;

		glm::quat m_rotInitial;
		glm::quat m_rotCurrent;

		glm::vec3 m_locRotRt;
		glm::vec3 m_locRotUp;
		glm::vec3 m_locRotFw;

		glm::vec3 m_inputVec;
		float m_inputMagnitude;

		float m_moveSpeed;
		glm::vec3 m_moveVec;
		glm::vec3 m_moveForward;
		glm::vec3 m_moveStrafe;

		glm::vec3 m_position;

		camera m_camera;

	public:

		fps_camera_controller();

		inline camera& get_camera() {
			return m_camera;
		}

		void on_window_resized(int _width, int _height);

		void on_mouse_button(int _button, int _action, int _mods);

		void on_cursor_moved(float _x, float _y);

		void on_key_input(int _key, int _action);

		void on_update(float _timeStep);

		void on_render();
	};

};