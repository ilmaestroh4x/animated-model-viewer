#include "shader.h"

#include "debug.h"

using namespace core;

static const GLchar* vertex_shader =
#ifdef __EMSCRIPTEN__
"#version 300 es\n"
"precision mediump float;\n"
#else
"#version 400 core\n"
#endif

"#define MAX_BONES 55\n"

"uniform mat4 u_mvp;"

"uniform bool u_has_bones;"
"uniform mat4 u_bones[MAX_BONES];"

"layout (location = 0) in  vec3 a_position;"
"layout (location = 1) in  vec3 a_normal;"
"layout (location = 2) in  vec2 a_texcoord;"
"layout (location = 3) in  vec4 a_bone_weights;"
"layout (location = 4) in ivec4 a_bone_ids;"

"out vec4 v_debug;"
"out vec2 v_uv;"

"mat4 calcSkin(in ivec4 boneIds, in vec4 boneWeights) {"
"	return ("
"		u_bones[boneIds[0]] * boneWeights[0] + "
"		u_bones[boneIds[1]] * boneWeights[1] + "
"		u_bones[boneIds[2]] * boneWeights[2] + "
"		u_bones[boneIds[3]] * boneWeights[3]   "
"	);"
"}"

"void main() {"
"	vec4 position   = vec4(a_position, 1.0);"
"	vec4 normal     = vec4(a_normal,   0.0);"

"	if (u_has_bones) {"
"		mat4 skinMat = calcSkin(a_bone_ids, a_bone_weights);"
"		position	 = skinMat * position;"
"		normal		 = skinMat * normal;"
"	}"

"	normal = vec4(normalize(normal.xyz), 0.0);"

"	v_debug = normal;"
"	v_uv = a_texcoord;"

"	gl_Position = u_mvp * position;"
"}";

static const GLchar* fragment_shader =
#ifdef __EMSCRIPTEN__
"#version 300 es\n"
"precision mediump float;\n"
#else
"#version 400 core\n"
#endif

"uniform sampler2D u_diffuse_texture;"

"in vec4 v_debug;"
"in vec2 v_uv;"

"out vec4 result;"

"void main() {"
"	result = vec4(texture(u_diffuse_texture, v_uv).rgb, 1.0);"
"}";

shader::shader() {
	m_program = gl::load_shader_program(vertex_shader, fragment_shader);

	m_mvp = gl(GetUniformLocation(m_program, "u_mvp"));
	m_normalMatrix = gl(GetUniformLocation(m_program, "u_normal_mat"));
	m_hasBones = gl(GetUniformLocation(m_program, "u_has_bones"));
	m_bones = gl(GetUniformLocation(m_program, "u_bones"));
	m_diffuseTexture = gl(GetUniformLocation(m_program, "u_diffuse_texture"));
}

void shader::begin() const {
	gl(UseProgram(m_program));
}

void shader::end() const {
	gl(UseProgram(0));
}