#pragma once

#include "../gl.h"

namespace core {

	class shader {

		GLuint m_program;
		GLint m_mvp;
		GLint m_normalMatrix;
		GLint m_hasBones;
		GLint m_bones;
		GLint m_diffuseTexture;

	public:

		shader();

		void begin() const;

		void end() const;

		inline GLint get_program() const {
			return m_program;
		}

		inline GLint get_mvp() const {
			return m_mvp;
		}

		inline GLint get_normal_matrix() const {
			return m_normalMatrix;
		}

		inline GLint get_has_bones() const {
			return m_hasBones;
		}

		inline GLint get_bones() const {
			return m_bones;
		}

		inline GLint get_diffuse_texture() const {
			return m_diffuseTexture;
		}
	};

};