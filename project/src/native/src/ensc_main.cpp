#ifdef __EMSCRIPTEN__

#include <emscripten/emscripten.h>
#include <emscripten/html5.h>
#include <GLES3/gl3.h>

#include "logger.h"

#include "core/engine.h"

int hash_c_string(const char* p, int s) {
    int result = 0;
    const int prime = 31;
    for (int i = 0; i < s; ++i) {
        if (p[i] == '\0') break;
        result = p[i] + (result * prime);
    }
    return result;
}

const float MAX_DELTA_TIME	 = 0.25f;
const float UPDATE_TICK_RATE = 0.01f;

float new_time = 0;
float cur_time = 0;
float delta_time = 0;
float global_time = 0;
float time_accumulator = 0;

core::engine* engine = nullptr;

bool cursor_lock = false;

void loop();

int main() {
    //https://kripken.github.io/emscripten-site/docs/api_reference/html5.h.html#c.EmscriptenWebGLContextAttributes.majorVersion
    EmscriptenWebGLContextAttributes attrs;
    emscripten_webgl_init_context_attributes(&attrs);

    //set webgl version to 2, glsl version to 3
    attrs.majorVersion = 2;

    EMSCRIPTEN_WEBGL_CONTEXT_HANDLE context = emscripten_webgl_create_context(nullptr, &attrs);
    if (context <= 0) {
        emscripten_force_exit(1);
        return 1;
    }

    if (emscripten_webgl_make_context_current(context) != EMSCRIPTEN_RESULT_SUCCESS) {
        emscripten_force_exit(1);
        return 1;
    }

    EmscriptenFullscreenStrategy strategy;
    strategy.scaleMode = EMSCRIPTEN_FULLSCREEN_SCALE_DEFAULT;
    strategy.canvasResolutionScaleMode = EMSCRIPTEN_FULLSCREEN_CANVAS_SCALE_STDDEF;
    strategy.filteringMode = EMSCRIPTEN_FULLSCREEN_FILTERING_DEFAULT;
    if (emscripten_enter_soft_fullscreen(nullptr, &strategy) != EMSCRIPTEN_RESULT_SUCCESS) {
        emscripten_force_exit(1);
        return 1;
    }

	LOG("ENSC_MAIN");
	LOG("GL_VERSION " << glGetString(GL_VERSION));

    engine = new core::engine;

    double initialWidth, initialHeight;
    emscripten_get_element_css_size(nullptr, &initialWidth, &initialHeight);
    //LOG("emscripten_get_element_css_size: " << w << ", " << h);
    engine->on_window_resized(initialWidth, initialHeight);

    emscripten_set_resize_callback(nullptr, nullptr, false, [](int _eventType, const EmscriptenUiEvent* _uiEvent, void* _userData)->EM_BOOL{
        //LOG("emscripten_set_resize_callback: " << _uiEvent->windowInnerWidth << ", " << _uiEvent->windowInnerHeight);
        engine->on_window_resized(_uiEvent->windowInnerWidth, _uiEvent->windowInnerHeight);
        return EM_TRUE;
    });

    emscripten_set_mousemove_callback(nullptr, nullptr, false, [](int _eventType, const EmscriptenMouseEvent* _mouseEvent, void* _userData)->EM_BOOL{
        //LOG("emscripten_set_mousemove_callback: " << _mouseEvent->canvasX << ", " << _mouseEvent->canvasY);
        //engine->on_cursor_moved(static_cast<float>(_mouseEvent->canvasX), static_cast<float>(_mouseEvent->canvasY));
        //LOG("emscripten_set_mousemove_callback: " << _mouseEvent->movementX << ", " << _mouseEvent->movementY);
        if (cursor_lock) {
            engine->on_cursor_moved(-static_cast<float>(_mouseEvent->movementX), -static_cast<float>(_mouseEvent->movementY));
        }
        return EM_TRUE;
    });

    emscripten_set_mousedown_callback(nullptr, nullptr, false, [](int _eventType, const EmscriptenMouseEvent* _mouseEvent, void* _userData)->EM_BOOL{
        if (_mouseEvent->button == 2) {
            cursor_lock = true;
            emscripten_request_pointerlock(nullptr, true);
        }
        return EM_TRUE;
    });
    emscripten_set_mouseup_callback(nullptr, nullptr, false, [](int _eventType, const EmscriptenMouseEvent* _mouseEvent, void* _userData)->EM_BOOL{
        if (_mouseEvent->button == 2) {
            cursor_lock = false;
            emscripten_exit_pointerlock();
        }
        return EM_TRUE;
    });

    //emscripten_set_mouseup_callback(nullptr, nullptr, false, ImGui_ImplEM_MouseCallback);
    //emscripten_set_wheel_callback(nullptr, nullptr, false, ImGui_ImplEM_ScrollCallback);
    emscripten_set_keydown_callback(nullptr, nullptr, false, [](int _eventType, const EmscriptenKeyboardEvent* _keyEvent, void* _userData)->EM_BOOL{
        if (cursor_lock) {
            int keyCode = hash_c_string(_keyEvent->code, 32);
            //LOG("type:" << _eventType << " code:" << keyCode);
            engine->on_key_input(keyCode, _eventType);
        }
        return EM_TRUE;
    });
    emscripten_set_keyup_callback(nullptr, nullptr, false, [](int _eventType, const EmscriptenKeyboardEvent* _keyEvent, void* _userData)->EM_BOOL{
        if (cursor_lock) {
            int keyCode = hash_c_string(_keyEvent->code, 32);
            //LOG("type:" << _eventType << " code:" << keyCode);
            engine->on_key_input(keyCode, _eventType);
        }
        return EM_TRUE;
    });


    cur_time = static_cast<float>(emscripten_get_now() / 1000.0f);
    emscripten_set_main_loop(loop, 0, 0);
    
    return 0;
}

void loop() {
    new_time = static_cast<float>(emscripten_get_now() / 1000.0f);
    delta_time = new_time - cur_time;

    if (delta_time > MAX_DELTA_TIME) {
        delta_time = MAX_DELTA_TIME;
    }

    cur_time = new_time;
    time_accumulator += delta_time;

    while (time_accumulator >= UPDATE_TICK_RATE) {
        time_accumulator -= UPDATE_TICK_RATE;
        global_time += UPDATE_TICK_RATE;

        engine->on_update(global_time, UPDATE_TICK_RATE);
    }

    engine->on_render(global_time);
}
#endif