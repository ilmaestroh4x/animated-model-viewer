#include "gl.h"

#ifdef _DEBUG
#include <iostream>
#define LOG(msg) std::cout << msg << std::endl
#else
#define LOG(msg)
#endif

void gl::error_check(const char* _file, long _line) {
	bool hasErrors = false;
	GLenum err;
	while ((err = glGetError()) != GL_NO_ERROR) {
		hasErrors = true;
		switch (err) {
		case GL_INVALID_ENUM:
			LOG("GL_ERROR: " << _file << ":" << _line << " GL_INVALID_ENUM");
			break;
		case GL_INVALID_VALUE:
			LOG("GL_ERROR: " << _file << ":" << _line << " GL_INVALID_VALUE");
			break;
		case GL_INVALID_OPERATION:
			LOG("GL_ERROR: " << _file << ":" << _line << " GL_INVALID_OPERATION");
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			LOG("GL_ERROR: " << _file << ":" << _line << " GL_INVALID_FRAMEBUFFER_OPERATION");
			break;
		case GL_OUT_OF_MEMORY:
			LOG("GL_ERROR: " << _file << ":" << _line << " GL_OUT_OF_MEMORY");
			break;
#ifndef __EMSCRIPTEN__
		case GL_STACK_UNDERFLOW:
			LOG("GL_ERROR: " << _file << ":" << _line << " GL_STACK_UNDERFLOW");
			break;
		case GL_STACK_OVERFLOW:
			LOG("GL_ERROR: " << _file << ":" << _line << " GL_STACK_OVERFLOW");
			break;
#endif
		}
	}
}

GLuint gl::create_shader(GLenum _type, const GLchar* _source) {
	GLuint shader = gl(CreateShader(_type));
	if (shader) {
		gl(ShaderSource(shader, 1, &_source, nullptr));
		gl(CompileShader(shader));

		GLint compileStatus;
		gl(GetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus));
		if (compileStatus == GL_FALSE) {
			GLint logLength;
			gl(GetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength));

			GLchar* infoLog = new GLchar[logLength];
			gl(GetShaderInfoLog(shader, logLength, &logLength, infoLog));

			LOG("GLSL_SHADER_ERROR: " << infoLog);
			delete[] infoLog;

			gl(DeleteShader(shader));
			shader = 0;
		}
	}
	return shader;
}

GLuint gl::create_program(GLuint _vertexShader, GLuint _fragmentShader) {
	GLuint program = gl(CreateProgram());
	if (program) {
		gl(AttachShader(program, _vertexShader));
		gl(AttachShader(program, _fragmentShader));

		gl(LinkProgram(program));

		GLint linkStatus;
		gl(GetProgramiv(program, GL_LINK_STATUS, &linkStatus));

		gl(DetachShader(program, _vertexShader));
		gl(DetachShader(program, _fragmentShader));

		if (linkStatus == GL_TRUE) {
			gl(ValidateProgram(program));
		} else {
			GLint logLength;
			gl(GetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength));

			GLchar* infoLog = new GLchar[logLength];
			gl(GetProgramInfoLog(program, logLength, &logLength, infoLog));

			LOG("GLSL_PROGRAM_ERROR: " << infoLog);
			delete[] infoLog;

			gl(DeleteProgram(program));
			program = 0;
		}
	}
	return program;
}

GLuint gl::load_shader_program(const GLchar* _vertexSource, const GLchar* _fragmentSource) {
	GLuint vs = gl::create_shader(GL_VERTEX_SHADER, _vertexSource);
	if (!vs) return 0;
	GLuint fs = gl::create_shader(GL_FRAGMENT_SHADER, _fragmentSource);
	if (!fs) return 0;
	return gl::create_program(vs, fs);
}