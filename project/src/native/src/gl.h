#pragma once

#ifdef __EMSCRIPTEN__
	#include <GLES3/gl3.h>
#else
	#include <GL/glew.h>
#endif

namespace gl {

	void error_check(const char* _file, long _line);

	GLuint create_shader(GLenum _type, const GLchar* _source);

	GLuint create_program(GLuint _vertexShader, GLuint _fragmentShader);

	GLuint load_shader_program(const GLchar* _vertexSource, const GLchar* _fragmentSource);

};

#ifdef _DEBUG
	#define gl(x) gl ## x; gl::error_check(__FILE__, __LINE__);
#else
	#define gl(x) gl ## x;
#endif