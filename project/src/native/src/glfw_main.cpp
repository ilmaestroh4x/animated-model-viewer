#ifndef __EMSCRIPTEN__

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "logger.h"

#include "core/engine.h"

#ifndef _DEBUG
	#ifdef _WIN32
		//prevent console window from showing up on release build (windows only).
		#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
	#endif
#endif

bool cursor_lock = false;
double cursor_lock_x = 0;
double cursor_lock_y = 0;
double cursor_free_x = 0;
double cursor_free_y = 0;
double cursor_prev_x = 0;
double cursor_prev_y = 0;

int main() {
	GLFWwindow* window;

	int winWidth = 1600;
	int winHeight = 900;

	glfwSetErrorCallback([](int error, const char* description) {
		LOG(error << " : " << description);
	});

	if (!glfwInit()) {
		LOG("glfwInit Failed");
		return -1;
	}

	glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	window = glfwCreateWindow(winWidth, winHeight, "3D Model Loader", nullptr, nullptr);
	if (!window) {
		LOG("glfwCreateWindow Failed");
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	glfwShowWindow(window);

	if (glewInit() != GLEW_OK) {
		LOG("glewInit Failed");
		return -1;
	}
	
	GLint glGetIntResult;
	glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS, &glGetIntResult);
	LOG("GL_MAX_VERTEX_UNIFORM_COMPONENTS : " << glGetIntResult);

	core::engine engine;

	/* Do an inital resize call to allow the engine to configure the projection matrices, etc. */
	engine.on_window_resized(winWidth, winHeight);

	/* Bind engine pointer to window for window event callbacks. */
	glfwSetWindowUserPointer(window, &engine);

	/* Directs any window resized changes to engine::on_window_resized */
	glfwSetWindowSizeCallback(window, [](GLFWwindow* _window, int _width, int _height) {
		static_cast<core::engine*>(glfwGetWindowUserPointer(_window))->on_window_resized(_width, _height);
	});

	/* Directs any mouse button changes to engine::on_mouse_button */
	glfwSetMouseButtonCallback(window, [](GLFWwindow* _window, int _button, int _action, int _mods) {
		switch (_button) {
		case GLFW_MOUSE_BUTTON_RIGHT:
			switch (_action) {
			case GLFW_PRESS:
				glfwGetCursorPos(_window, &cursor_free_x, &cursor_free_y);
				glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
				glfwSetCursorPos(_window, cursor_lock_x, cursor_lock_y);
				//cursor_prev_x = cursor_lock_x;
				//cursor_prev_y = cursor_lock_y;
				cursor_lock = true;

				break;
			case GLFW_RELEASE:
				glfwGetCursorPos(_window, &cursor_lock_x, &cursor_lock_y);
				glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
				glfwSetCursorPos(_window, cursor_free_x, cursor_free_y);
				cursor_lock = false;
				break;
			}
			break;
		}
		static_cast<core::engine*>(glfwGetWindowUserPointer(_window))->on_mouse_button(_button, _action, _mods);
	});

	/* Directs any mouse pos changes to engine::on_cursor_moved */
	glfwSetCursorPosCallback(window, [](GLFWwindow* _window, double _x, double _y) {
		if (cursor_lock) {
			float dx = static_cast<float>(cursor_prev_x - _x);
			float dy = static_cast<float>(cursor_prev_y - _y);
			if (dx > 150 || dy > 150) {
				dx = 0;
				dy = 0;
			}
			static_cast<core::engine*>(glfwGetWindowUserPointer(_window))->on_cursor_moved(dx, dy);
			cursor_prev_x = _x;
			cursor_prev_y = _y;
		}
	});

	/* Directs any key state changes to engine::on_key_input */
	glfwSetKeyCallback(window, [](GLFWwindow* _window, int _key, int _scancode, int _action, int _mods) {
		if (_key == GLFW_KEY_ESCAPE && _action == GLFW_RELEASE) {
			glfwSetWindowShouldClose(_window, true);
		} else {
			static_cast<core::engine*>(glfwGetWindowUserPointer(_window))->on_key_input(_key, _action);
		}
	});

	const float MAX_DELTA_TIME	 = 0.25f;
	const float UPDATE_TICK_RATE = 0.01f;

	float newTime = 0;
	float deltaTime = 0;
	float curTime = static_cast<float>(glfwGetTime());
	float globalTime = 0;
	float timeAccumulator = 0;

	while (!glfwWindowShouldClose(window)) {
		newTime = static_cast<float>(glfwGetTime());
		deltaTime = newTime - curTime;

		if (deltaTime > MAX_DELTA_TIME) {
			deltaTime = MAX_DELTA_TIME;
		}

		curTime = newTime;
		timeAccumulator += deltaTime;

		while (timeAccumulator >= UPDATE_TICK_RATE) {
			timeAccumulator -= UPDATE_TICK_RATE;
			globalTime += UPDATE_TICK_RATE;

			/* Update */
			engine.on_update(globalTime, UPDATE_TICK_RATE);
		}

		/* Render */
		engine.on_render(globalTime);

		//TODO gui

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}
#endif