#pragma once

#ifdef _DEBUG
	#include <iostream>
	#define LOG(msg) std::cout << __FILE__ << " L" << __LINE__ << ": " << msg << std::endl
#else
	#define LOG(msg)
#endif