//#ifdef __EMSCRIPTEN__
//    #include "ensc_main.h"
//#else
//    #include "glfw_main.h"
//#endif

/*
void loop() {
    newTime = emscripten_get_now() / 1000.0;
    deltaTime = newTime - curTime;

    if (deltaTime > MAX_DELTA_TIME) {
        deltaTime = MAX_DELTA_TIME;
    }

    curTime = newTime;
    timeAccumulator += deltaTime;

    while (timeAccumulator >= UPDATE_TICK_RATE) {
        timeAccumulator -= UPDATE_TICK_RATE;
        globalTime += UPDATE_TICK_RATE;

        //engine->on_update(globalTime, UPDATE_TICK_RATE);
    }

    engine->on_render();

    ImGui_ImplEMGL3_NewFrame();

    engine->on_gui();

    ImGui::Render();
    ImGui_ImplEMGL3_RenderDrawData(ImGui::GetDrawData());
}

void ImGui_InitTheme() {
    //Switch the default ImGui font with Roboto Regular 16px.
	ImFontConfig highDpiFontConfig;
	highDpiFontConfig.OversampleH = 4;
	highDpiFontConfig.OversampleV = 3;
	highDpiFontConfig.PixelSnapH = true;

	ImGuiIO& io = ImGui::GetIO();
	io.Fonts->AddFontFromFileTTF("src/assets/Roboto-Regular.ttf", 16, &highDpiFontConfig);
	io.Fonts->Build();

	//Config ImGui styling.
	ImGuiStyle& style = ImGui::GetStyle();

	// - Border Width
	style.WindowBorderSize = 1.0f;
	style.ChildBorderSize  = 0.0f;
	style.PopupBorderSize  = 1.0f;
	style.FrameBorderSize  = 0.0f;

	// - Rounding
	style.WindowRounding	= 0.0f;
	style.ChildRounding		= 0.0f;
	style.FrameRounding		= 0.0f;
	style.ScrollbarRounding = 0.0f;
	style.GrabRounding		= 0.0f;
	
	// - Colors
	ImVec4* colors = style.Colors;
	colors[ImGuiCol_Text] = ImVec4(0.77f, 0.8f, 0.85f, 1.0f);;
	colors[ImGuiCol_WindowBg] = ImVec4(0.16f, 0.17f, 0.2f, 1.0f);
	colors[ImGuiCol_PopupBg] = ImVec4(0.2f, 0.22f, 0.26f, 1.0f);
	colors[ImGuiCol_Border] = ImVec4(1.0f, 1.0f, 1.0f, 0.25f);

	colors[ImGuiCol_TitleBg] = ImVec4(0.13f, 0.15f, 0.17f, 1.0f);
	colors[ImGuiCol_TitleBgActive] = ImVec4(0.36f, 0.39f, 0.44f, 1.0f);

	colors[ImGuiCol_MenuBarBg] = ImVec4(0.2f, 0.22f, 0.26f, 1.0f);

	colors[ImGuiCol_ModalWindowDarkening] = ImVec4(0.0f, 0.0f, 0.0f, 0.5f);

	colors[ImGuiCol_ResizeGrip] = ImVec4(0.2f, 0.22f, 0.26f, 1.0f);
	colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.36f, 0.39f, 0.44f, 1.0f);
	colors[ImGuiCol_ResizeGripActive] = ImVec4(0.36f, 0.39f, 0.44f, 1.0f);
	

	colors[ImGuiCol_FrameBg] = ImVec4(0.4f, 0.44f, 0.52f, 0.25f);
	colors[ImGuiCol_FrameBgHovered] = ImVec4(0.4f, 0.44f, 0.52f, 0.5f);
	colors[ImGuiCol_FrameBgActive] = ImVec4(0.4f, 0.44f, 0.52f, 0.5f);


	colors[ImGuiCol_SliderGrab] = ImVec4(0.175f, 0.325f, 0.455f, 1.0f);
	colors[ImGuiCol_SliderGrabActive] = ImVec4(0.35f, 0.65f, 0.91f, 1.0f);

	colors[ImGuiCol_Button] = ImVec4(0.175f, 0.325f, 0.455f, 1.0f);
	colors[ImGuiCol_ButtonHovered] = ImVec4(0.35f, 0.65f, 0.91f, 1.0f);
	colors[ImGuiCol_ButtonActive] = ImVec4(0.35f, 0.65f, 0.91f, 1.0f);

	colors[ImGuiCol_Header] = ImVec4(0.175f, 0.325f, 0.455f, 1.0f);
	colors[ImGuiCol_HeaderHovered] = ImVec4(0.35f, 0.65f, 0.91f, 1.0f);
	colors[ImGuiCol_HeaderActive] = ImVec4(0.35f, 0.65f, 0.91f, 1.0f);

	colors[ImGuiCol_Separator] = ImVec4(0.175f, 0.325f, 0.455f, 1.0f);
	colors[ImGuiCol_SeparatorHovered] = ImVec4(0.35f, 0.65f, 0.91f, 1.0f);
	colors[ImGuiCol_SeparatorActive] = ImVec4(0.35f, 0.65f, 0.91f, 1.0f);

	colors[ImGuiCol_ResizeGrip] = ImVec4(0.175f, 0.325f, 0.455f, 1.0f);
	colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.35f, 0.65f, 0.91f, 1.0f);
	colors[ImGuiCol_ResizeGripActive] = ImVec4(0.35f, 0.65f, 0.91f, 1.0f);
	

	colors[ImGuiCol_ScrollbarBg] = ImVec4(0.13f, 0.15f, 0.17f, 1.0f);
	colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.2f, 0.22f, 0.26f, 1.0f);
	colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.36f, 0.39f, 0.44f, 1.0f);
	colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.36f, 0.39f, 0.44f, 1.0f);
}
*/