#pragma once

#include <string>
#include <vector>

#include <cereal/access.hpp>

#include "animation_channel.h"

namespace model {

	struct animation {

		std::string name;
		float duration;
		float ticks_per_second;
		std::vector<animation_channel> channels;

	private:

		friend class cereal::access;

		template<class Archive>
		void serialize(Archive& archive) {
			archive(name, duration, ticks_per_second, channels);
		}

	};

};