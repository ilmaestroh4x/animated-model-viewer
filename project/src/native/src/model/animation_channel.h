#pragma once

#include <vector>

#include <cereal/access.hpp>

#include "animation_vec3_key.h"
#include "animation_quat_key.h"

namespace model {

	struct animation_channel {

		std::vector<animation_vec3_key> position_keys;
		std::vector<animation_quat_key> rotation_keys;
		std::vector<animation_vec3_key> scale_keys;
		int node_id;

	private:

		friend class cereal::access;

		template<class Archive>
		void serialize(Archive& archive) {
			archive(position_keys, rotation_keys, scale_keys, node_id);
		}

	};

};