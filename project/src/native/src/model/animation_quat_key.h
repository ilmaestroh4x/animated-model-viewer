#pragma once

#include <glm/gtc/quaternion.hpp>

#include <cereal/access.hpp>

namespace model {

	struct animation_quat_key {

		float time;
		glm::quat value;
		
	private:

		friend class cereal::access;

		template<class Archive>
		void serialize(Archive& archive) {
			archive(time, value);
		}

	};

};