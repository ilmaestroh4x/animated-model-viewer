#pragma once

#include <glm/glm.hpp>

#include <cereal/access.hpp>

namespace model {

	struct animation_vec3_key {

		float time;
		glm::vec3 value;

	private:

		friend class cereal::access;

		template<class Archive>
		void serialize(Archive& archive) {
			archive(time, value);
		}

	};

};