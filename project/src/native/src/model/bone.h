#pragma once

#include <glm/glm.hpp>

#include <cereal/access.hpp>

namespace model {

	struct bone {

		unsigned int id;
		int node_id;
		glm::mat4 offset;

	private:

		friend class cereal::access;

		template<class Archive>
		void serialize(Archive& archive) {
			archive(id, node_id, offset);
		}

	};

};