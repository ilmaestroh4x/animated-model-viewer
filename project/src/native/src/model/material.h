#pragma once

#include <cereal/access.hpp>

namespace model {

	struct material {

		int diffuse_tex_id;

	private:

		friend class cereal::access;

		template<class Archive>
		void serialize(Archive& archive) {
			archive(diffuse_tex_id);
		}

	};

};