#pragma once

#include <vector>

#include <glm/glm.hpp>

#include <cereal/access.hpp>

#include "vertex.h"
#include "bone.h"

namespace model {

	struct mesh {

		std::vector<vertex> vertices;
		std::vector<unsigned int> indices;
		std::vector<bone> bones;
		unsigned int material_id;
		int node_id;
		
	private:

		friend class cereal::access;

		template<class Archive>
		void serialize(Archive& archive) {
			archive(vertices, indices, bones, material_id, node_id);
		}

	};

};