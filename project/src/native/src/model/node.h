#pragma once

#include <string>
#include <vector>

#include <glm/glm.hpp>

#include <cereal/access.hpp>

namespace model {

	struct node {

		std::string name; //used only by assimp (not serealized)
		unsigned int this_id;	  //only used to check if root, use bool is_root instead?
		unsigned int parent_id;
		glm::mat4 transformation;
		std::vector<unsigned int> mesh_ids; // used only by assimp?

	private:

		friend class cereal::access;

		template<class Archive>
		void serialize(Archive& archive) {
			archive(this_id, parent_id, transformation, mesh_ids);
		}

	};

};