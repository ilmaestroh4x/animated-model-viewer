#pragma once

#include <vector>

#include <cereal/access.hpp>

#include "texture.h"
#include "material.h"
#include "node.h"
#include "animation.h"
#include "mesh.h"

namespace model {

	struct scene {

		std::vector<texture> textures;
		std::vector<material> materials;
		std::vector<node> nodes;
		std::vector<animation> animations;
		std::vector<mesh> meshes;

	private:

		friend class cereal::access;

		template<class Archive>
		void serialize(Archive& archive) {
			archive(textures, materials, nodes, animations, meshes);
		}

	};

};