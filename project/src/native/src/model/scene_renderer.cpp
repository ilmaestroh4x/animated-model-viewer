#include "scene_renderer.h"

#include <cmath>

#include <string>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <stb_image/stb_image.h>

#include "../logger.h"

using namespace model;

void scene_renderer::calc_key_lerp(
	const std::vector<animation_key>& _keys,
	float _time,
	int& _prevKeyId,
	int& _nextKeyId,
	float& _delta
) {
	auto keyCount = _keys.size() - 1;
	if (keyCount > 1) {
		for (decltype(keyCount) i = _prevKeyId; i <= keyCount; i++) {
			if (_keys[i].time <= _time) {
				_prevKeyId = i;
			} else {
				_nextKeyId = i;
				break;
			}
		}
	} else {
		_prevKeyId = 0;
		_nextKeyId = 0;
	}

	if (_prevKeyId != _nextKeyId) {
		if (_prevKeyId > _nextKeyId) {
			int temp   = _prevKeyId;
			_prevKeyId = _nextKeyId;
			_nextKeyId = temp;
		}

		float t0 = _keys[_prevKeyId].time;
		float t1 = _keys[_nextKeyId].time;

		_delta = (_time - t0) / (t1 - t0);
	} else {
		_prevKeyId = 0;
		_delta = 1;
	}
}

scene_renderer::scene_renderer(
	const scene & _scene
) {

	// Textures
	m_textures.reserve(_scene.textures.size());
	for (const auto& texture_ : _scene.textures) {
		const auto file = texture_.path + texture_.name;

		GLuint id;

		int width, height, comps;
		auto img_data = stbi_load(file.c_str(), &width, &height, &comps, STBI_rgb_alpha);

		gl(GenTextures(1, &id));
		gl(BindTexture(GL_TEXTURE_2D, id));

		gl(PixelStorei(GL_UNPACK_ALIGNMENT, 1)); //Not sure if this hurts anythig. (Should fix other possible issues)
		gl(TexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img_data));

		gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR));
		gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
		gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
		gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));

		// Must be specified if using mipmap in min filter
		gl(GenerateMipmap(GL_TEXTURE_2D));

		gl(BindTexture(GL_TEXTURE_2D, 0));

		stbi_image_free(img_data);

		m_textures.push_back(id);
	}

	// Materials
	m_materials.reserve(_scene.materials.size());
	for (const auto& material_ : _scene.materials) {
		m_materials.push_back({
			m_textures[material_.diffuse_tex_id]
		});
	}

	// Animations
	m_animations.reserve(_scene.animations.size());
	for (const auto& animation_ : _scene.animations) {
		animation a;

		a.name = animation_.name;
		a.duration = animation_.duration;
		a.ticks_per_second = animation_.ticks_per_second;

		a.channels.reserve(animation_.channels.size());
		for (const auto& channel_ : animation_.channels) {
			animation_channel ac;

			ac.node_id = channel_.node_id;

			ac.prev_position_key_id = 0;
			ac.position_keys.reserve(channel_.position_keys.size());
			for (const auto& key_ : channel_.position_keys) {
				ac.position_keys.push_back({key_.time, key_.value});
			}

			ac.prev_rotation_key_id = 0;
			ac.rotation_keys.reserve(channel_.rotation_keys.size());
			for (const auto& key_ : channel_.rotation_keys) {
				ac.rotation_keys.push_back({ key_.time, key_.value });
			}

			ac.prev_scale_key_id = 0;
			ac.scale_keys.reserve(channel_.scale_keys.size());
			for (const auto& key_ : channel_.scale_keys) {
				ac.scale_keys.push_back({ key_.time, key_.value });
			}

			a.channels.push_back(ac);
		}

		m_animations.push_back(a);
	}

	// Nodes
	m_nodes.reserve(_scene.nodes.size());
	for (const auto& node_ : _scene.nodes) {
		m_nodes.push_back({
			node_.this_id,
			node_.parent_id,
			node_.transformation,
			node_.transformation
		});
	}

	// Meshes
	m_meshes.reserve(_scene.meshes.size());
	for (const auto& mesh_ : _scene.meshes) {
		mesh m;

		m.node_id = mesh_.node_id;
		m.material_id = mesh_.material_id;

		m.bones.reserve(mesh_.bones.size());
		for (const auto& bone : mesh_.bones) {
			m.bones.push_back({
				bone.id,
				bone.node_id,
				bone.offset
			});
		}

		m.bone_transforms = std::vector<glm::mat4>(m.bones.size(), glm::mat4());

		m.num_indices = mesh_.indices.size();

		gl(GenVertexArrays(1, &m.vao));
		gl(GenBuffers(1, &m.vbo));
		gl(GenBuffers(1, &m.ebo));

		gl(BindVertexArray(m.vao));

		gl(BindBuffer(GL_ARRAY_BUFFER, m.vbo));
		gl(BufferData(GL_ARRAY_BUFFER, mesh_.vertices.size() * sizeof(vertex), mesh_.vertices.data(), GL_STATIC_DRAW));

		gl(BindBuffer(GL_ELEMENT_ARRAY_BUFFER, m.ebo));
		gl(BufferData(GL_ELEMENT_ARRAY_BUFFER, mesh_.indices.size() * sizeof(unsigned int), mesh_.indices.data(), GL_STATIC_DRAW));

		// vertex positions (vec3)
		gl(EnableVertexAttribArray(0));
		gl(VertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), reinterpret_cast<void*>(offsetof(vertex, position))));

		// vertex normals (vec3)
		gl(EnableVertexAttribArray(1));
		gl(VertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, sizeof(vertex), reinterpret_cast<void*>(offsetof(vertex, normal))));

		// vertex texture coords (vec2)
		gl(EnableVertexAttribArray(2));
		gl(VertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), reinterpret_cast<void*>(offsetof(vertex, texcoord))));

		if (!m.bones.empty()) {

			// vertex bone weights (vec4)
			gl(EnableVertexAttribArray(3));
			gl(VertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(vertex), reinterpret_cast<void*>(offsetof(vertex, bone_weights))));

			// vertex bone ids (ivec4)
			gl(EnableVertexAttribArray(4));
			gl(VertexAttribIPointer(4, 4, GL_INT, sizeof(vertex), reinterpret_cast<void*>(offsetof(vertex, bone_ids))));

		}

		gl(BindVertexArray(0));

		m_meshes.push_back(m);
	}

}

void scene_renderer::animate(
	unsigned int _animationId,
	float _time
) {
	if (m_animations.size() < _animationId) return;


	auto& animation_ = m_animations[_animationId];

	_time = fmod(_time * animation_.ticks_per_second, animation_.duration);

	int next;
	float delta;

	for (auto& channel_ : animation_.channels) {
		auto& node_ = m_nodes[channel_.node_id];

		glm::mat4& t = node_.local_transform;

		t = glm::mat4(); //identity

		calc_key_lerp(channel_.position_keys, _time, channel_.prev_position_key_id, next, delta);
		if (delta == 1) {
			t = glm::translate(t, channel_.position_keys[next].vec3);
		} else {
			t = glm::translate(t, glm::mix(
				channel_.position_keys[channel_.prev_position_key_id].vec3,
				channel_.position_keys[next].vec3,
				delta
			));
		}

		calc_key_lerp(channel_.rotation_keys, _time, channel_.prev_rotation_key_id, next, delta);
		if (delta == 1) {
			const glm::quat& rot = channel_.rotation_keys[next].quat;
			t = glm::rotate(t, glm::angle(rot), glm::axis(rot));
		} else {
			glm::quat rot_lerp = glm::slerp(
				channel_.rotation_keys[channel_.prev_rotation_key_id].quat,
				channel_.rotation_keys[next].quat,
				delta
			);
			t = glm::rotate(t, glm::angle(rot_lerp), glm::axis(rot_lerp));
		}

		calc_key_lerp(channel_.scale_keys, _time, channel_.prev_scale_key_id, next, delta);
		if (delta == 1) {
			t = glm::scale(t, channel_.scale_keys[next].vec3);
		} else {
			t = glm::scale(t, glm::mix(
				channel_.scale_keys[channel_.prev_scale_key_id].vec3,
				channel_.scale_keys[next].vec3,
				delta
			));
		}
	}

}

void scene_renderer::render(
	const glm::mat4& _viewProjMat,
	const core::shader& _shader
) {

	for (auto& node_ : m_nodes) {
		if (node_.parent_id == node_.id) {
			node_.cached_transform = node_.local_transform;
		} else {
			node_.cached_transform = m_nodes[node_.parent_id].cached_transform * node_.local_transform;
		}
	}

    gl(UseProgram(_shader.get_program()));
	for (auto& mesh_ : m_meshes) {

		glm::mat4 mvp = _viewProjMat;
		
		if (mesh_.node_id >= 0) {
			mvp = mvp * m_nodes[mesh_.node_id].cached_transform;
		}
		
		//vertex shader
		gl(UniformMatrix4fv(_shader.get_mvp(), 1, GL_FALSE, glm::value_ptr(mvp)));

		auto& bones = mesh_.bones;
		auto& bone_transforms = mesh_.bone_transforms;

		if (bones.empty()) {
			gl(Uniform1i(_shader.get_has_bones(), GL_FALSE));
		} else {
			for (auto& bone_ : bones) {
				bone_transforms[bone_.id] = m_nodes[bone_.node_id].cached_transform * bone_.offset;
			}
			gl(Uniform1i(_shader.get_has_bones(), GL_TRUE));
			gl(UniformMatrix4fv(_shader.get_bones(), bone_transforms.size(), GL_FALSE, reinterpret_cast<GLfloat*>(glm::value_ptr(bone_transforms[0]))));
		}

		auto& mat = m_materials[mesh_.material_id];

		//fragment shader
		gl(ActiveTexture(GL_TEXTURE0));
		gl(Uniform1i(_shader.get_diffuse_texture(), 0));
		gl(BindTexture(GL_TEXTURE_2D, mat.diffuse_texture));

		gl(BindVertexArray(mesh_.vao));
		gl(DrawElements(GL_TRIANGLES, mesh_.num_indices, GL_UNSIGNED_INT, 0));
		//gl(BindVertexArray(0));

	}
	//_shader.end();

}
