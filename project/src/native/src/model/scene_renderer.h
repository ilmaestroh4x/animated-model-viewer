#pragma once

#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "../gl.h"
#include "../core/shader.h"

#include "scene.h"

namespace model {

	class scene_renderer {

		struct material {
			GLuint diffuse_texture;
		};

		struct animation_key {
			float time;
			union {
				glm::quat quat;
				glm::vec3 vec3;
			};

			animation_key(
				const float _time,
				const glm::quat _value
			) :
				time{_time},
				quat{_value} {
			}

			animation_key(
				const float _time,
				const glm::vec3 _value
			) :
				time{_time},
				vec3{_value} {
			}
		};

		struct animation_channel {
			int node_id;
			int prev_position_key_id;
			std::vector<animation_key> position_keys;
			int prev_rotation_key_id;
			std::vector<animation_key> rotation_keys;
			int prev_scale_key_id;
			std::vector<animation_key> scale_keys;
		};

		struct animation {
			std::string name;
			float duration;
			float ticks_per_second;
			std::vector<animation_channel> channels;
		};

		struct node {
			unsigned int id;
			unsigned int parent_id;
			glm::mat4 local_transform;
			glm::mat4 cached_transform;
		};

		struct bone {
			unsigned int id;
			int node_id;
			glm::mat4 offset;
		};

		struct mesh {
			int node_id;
			int material_id;
			std::vector<bone> bones;
			std::vector<glm::mat4> bone_transforms;
			GLsizei num_indices;
			GLuint vao;
			GLuint vbo;
			GLuint ebo;
		};
		
		std::vector<GLuint> m_textures;
		std::vector<material> m_materials;
		std::vector<animation> m_animations;
		std::vector<node> m_nodes;
		std::vector<mesh> m_meshes;

		void calc_key_lerp(
			const std::vector<animation_key>& _keys,
			float _time,
			int& _prevKeyId,
			int& _nextKeyId,
			float& _delta
		);

	public:

		scene_renderer(
			const scene& _scene
		);

		void animate(
			unsigned int _animationId,
			float _time
		);

		void render(
			const glm::mat4& _viewProjMat,
			const core::shader& _shader
		);

	};

};