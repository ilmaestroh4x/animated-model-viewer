#pragma once

#include <string>

#include <cereal/access.hpp>

#include "user_data.h"

namespace model {

	struct texture {

		std::string path;
		std::string name;

	private:

		friend class cereal::access;

		template <class Archive>
		void save(Archive& _archive) const {
			_archive(name);
		}

		template<class Archive>
		void load(Archive& _archive) {
			//user_data userData = cereal::get_user_data<user_data>(_archive);
			path = "src/assets/";//*/userData.get_file_path();

			_archive(name);
		}

	};

};