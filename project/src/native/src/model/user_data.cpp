#include "user_data.h"

#include "utils.h"

using namespace model;

user_data::user_data(
	const std::string& _path
) :
	path{_path} {

	auto pathParts = utils::split_string(_path, {'\\', '/', '.'});
	auto pathPartsLength = pathParts.size();

	if (pathPartsLength >= 2) {
		for (decltype(pathPartsLength) i = 0; i < pathPartsLength - 2; i++) {
			file_path += pathParts[i] + "/";
		}

		file_name = pathParts[pathPartsLength - 2];
		file_extension = pathParts[pathPartsLength - 1];
	} else {
		file_name = "";
		file_extension = "";
	}
}