#pragma once

#include <string>

namespace model {

	struct user_data {

		const std::string path;

	private:

		std::string file_path;
		std::string file_name;
		std::string file_extension;

	public:

		user_data(
			const std::string& _path
		);

		inline const std::string& get_file_path() const {
			return file_path;
		}

		inline const std::string& get_file_name() const {
			return file_name;
		}

		inline const std::string& get_file_extension() const {
			return file_extension;
		}

	};

};