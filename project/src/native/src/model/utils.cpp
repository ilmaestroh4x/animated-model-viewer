#include "utils.h"

#include <sstream>

using namespace model;

std::vector<std::string> utils::split_string(const std::string& _sourceString, const std::set<char>& _delimiters) {
	std::vector<std::string> result;

	char const* pch = _sourceString.c_str();
	char const* start = pch;
	for (; *pch; ++pch) {
		if (_delimiters.find(*pch) != _delimiters.end()) {
			if (start != pch) {
				std::string str(start, pch);
				result.push_back(str);
			} else {
				result.push_back("");
			}
			start = pch + 1;
		}
	}
	result.push_back(start);

	return result;
}