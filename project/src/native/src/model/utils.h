#pragma once

#include <set>
#include <vector>

namespace model {

	namespace utils {

		std::vector<std::string> split_string(const std::string& str, const std::set<char>& delimiters);

	};

};