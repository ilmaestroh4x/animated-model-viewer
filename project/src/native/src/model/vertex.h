#pragma once

#include <glm/glm.hpp>

#include <cereal/access.hpp>

namespace model {

	struct vertex {

		glm::vec3  position;
		glm::vec3  normal;
		glm::vec2  texcoord;
		glm::vec4  bone_weights;
		glm::ivec4 bone_ids;

	private:

		friend class cereal::access;

		template<class Archive>
		void serialize(Archive& archive) {
			archive(position, normal, texcoord, bone_weights, bone_ids);
		}

	};

};